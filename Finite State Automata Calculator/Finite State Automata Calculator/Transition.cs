﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finite_State_Automata_Calculator
{
    /// <summary>
    /// Stores the origin state, the target state and the letter it outputs when the transition is used.
    /// </summary>
    [Serializable]
    public class Transition
    {
        /// <summary>
        /// The state this transition points to.
        /// </summary>
        public State TargetState { get; private set; }

        /// <summary>
        /// The letter to be added to the word when traversing this transition.
        /// </summary>
        public char Letter { get; private set; }

        /// <summary>
        /// The letter to be added to the stack when traversing this transition.
        /// </summary>
        public char StackLetterToAdd { get; private set; }

        /// <summary>
        /// The letter to be removed from the stack when traversing this transition.
        /// </summary>
        public char StackLetterToRemove { get; private set; }

        /// <summary>
        /// Create a new transition with a state to go to and a letter to add when traversing this transition. <see cref="StackLetterToAdd"/> and <see cref="StackLetterToRemove"/> will be the empty character.
        /// </summary>
        /// <param name="targetState">The state this transition goes to.</param>
        /// <param name="letter">The letter to add to the word when traversing this transition.</param>
        public Transition(State targetState, char letter)
        {
            TargetState = targetState;
            Letter = letter;
            StackLetterToAdd = '\0';
            StackLetterToRemove = '\0';
        }

        /// <summary>
        /// Create a new transition with a state to go to and a letter to add, as well as the letters to add to and remove from the stack when traversing this transition. Only applies to PDA's.
        /// </summary>
        /// <param name="targetState">The state this transition goes to.</param>
        /// <param name="letter">The letter to add to the word when traversing this transition.</param>
        /// <param name="stackLetterToAdd">The letter to add to the stack when traversing this transition. Only applies to PDA's.</param>
        /// <param name="stackLetterToRemove">The letter to remove from the stack when traversing this transition. Only applies to PDA's.</param>
        public Transition(State targetState, char letter, char stackLetterToAdd, char stackLetterToRemove)
        {
            TargetState = targetState;
            Letter = letter;
            StackLetterToAdd = stackLetterToAdd;
            StackLetterToRemove = stackLetterToRemove;
        }
    }
}
